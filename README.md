# Hunting The Bismarck
Hunting the Bismarck is a multiplayer, turn-based, battleship game written in Javascript. It uses
Node JS as server, socket.io for bidirectional comunication, Express for file handling and AJAX for
some minimal function such as username availability check duriring registration.
Socket.io is also used to implement a minimal chat messaging service between the players.

Hunting the Bismarck uses MySql DBMS system to store some informations about the players, such as
total score, highest score during a single game and number of won game.

![screen](/uploads/5c80efdcbb726bb9e79dca13135081e9/screen.png)
